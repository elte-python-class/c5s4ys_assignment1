class Protein:
    def __init__(self, id, name, os, ox, gn, pe, sv, aa):
        self.id = id
        self.name = name
        self.os = os
        self.ox = ox
        self.gn = gn
        self.pe = pe
        self.sv = sv
        self.aa = aa
        self.size = len(aa)
        self.aminoacidcounts = {}
        for char in self.aa:
            if self.aminoacidcounts.get(char) == None:
                self.aminoacidcounts[char] = 1
            else:
                self.aminoacidcounts[char] += 1

    def __repr__(self):
        return self.name + " id: " + self.id
    
    def __eq__(self, other):
        return self.size == other.size
    
    def __ne__(self, other):
        return self.size != other.size

    def __lt__(self, other):
        return self.size < other.size

    def __le__(self, other):
        return self.size <= other.size

    def __gt__(self, other):
        return self.size > other.size

    def __ge__(self, other):
        return self.size >= other.size
    
def load_fasta(file):
    data = open(file,"r")
    #split by >
    raw_list = data.read().rsplit('>')
    #remove first empty element
    raw_list = raw_list[1:]
    rows_without_pipes_and_line_breaks = []
    list_of_splitted_rows=[]
    proteins_raw = []
    #remove not necessary substrings
    for line in raw_list:
        temp = line.replace("|", " ")
        temp = temp.replace("\n", " ")
        temp = temp.replace("sp ", "")
        rows_without_pipes_and_line_breaks.append(temp)
    #split to list of lists
    for row in rows_without_pipes_and_line_breaks:
        list_of_splitted_rows.append(row.split())
    for prot in list_of_splitted_rows:
        temprow = []
        tempString = ''
        #id
        temprow.append(prot[0])
        i=1
        #name
        while prot[i].startswith('OS') != True:
            tempString += prot[i]
            tempString += " "
            i+=1
        tempString = tempString[:-1]
        temprow.append(tempString)
        tempString = ""
        #OS
        while prot[i].startswith('OX') != True:
            tempString += prot[i]
            tempString += " "
            i+=1
        tempString = tempString[:-1]
        temprow.append(tempString[3:])
        tempString = ""
        #OX
        while prot[i].startswith('GN') != True:
            tempString += prot[i]
            i+=1
        temprow.append(int(tempString[3:]))
        tempString = ""
        #GN
        while prot[i].startswith('PE') != True:
            tempString += prot[i]
            i+=1
        temprow.append(tempString[3:])
        tempString = ""
        #PE
        while prot[i].startswith('SV') != True:
            tempString += prot[i]
            i+=1
        temprow.append(int(tempString[3:]))
        tempString = ""
        #SV
        temprow.append(int(prot[i][3:]))
        #aa
        i+=1
        while i<len(prot):
            tempString += prot[i]
            i+=1
        temprow.append(tempString)
        tempString = ""
        proteins_raw.append(temprow)
    list_of_proteins = []
    for protein in proteins_raw:
        list_of_proteins.append(Protein(protein[0], protein[1], protein[2], protein[3], protein[4], protein[5], protein[6], protein[7]))
    return list_of_proteins
    
def sort_proteins_pe(proteins):
    sortedProtens = sorted(proteins, key=lambda x: x.pe, reverse=True)
    return sortedProtens

def sort_proteins_aa(proteins, code):
    sortedProtens = sorted(proteins, key=lambda x: x.aa.count(code), reverse=True)
    return sortedProtens

def find_protein_with_motif(proteins,motif):
    proteins_with_motif = []
    for p in proteins:
        if p.aa.find(motif) != -1:
            proteins_with_motif.append(p)
    return proteins_with_motif
